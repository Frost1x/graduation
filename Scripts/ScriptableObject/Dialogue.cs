﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Dialogue", menuName = "Dialogue/Dialogue", order = 0)]
public class Dialogue : ScriptableObject
{
    public List<DialogueSegment> segments;
}
