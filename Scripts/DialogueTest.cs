﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class DialogueTest : MonoBehaviour
{
    public GameObject canvas;
    private GameObject dialogueUI;
    private GameObject interactUI;
    public Dialogue activeDialogue;
    private DialogueWindow dialogueWindow;
    private GameObject player;
    private CharacterMover mover;


    private void Awake()
    {
       player = GameObject.FindGameObjectWithTag("Player");
       mover = player.GetComponent<CharacterMover>();
       dialogueUI = canvas.transform.GetChild(0).gameObject;
       interactUI = canvas.transform.GetChild(1).gameObject;
       dialogueWindow = dialogueUI.GetComponent<DialogueWindow>();
    }

    private void OnTriggerEnter(Collider other)
    {
        interactUI.SetActive(true);
    }

    private void OnTriggerStay(Collider other)
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            dialogueUI.SetActive(true);
            interactUI.SetActive(false);
            dialogueWindow.ExecuteDialogue(activeDialogue);
            mover.StopPlayer();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        interactUI.SetActive(false);
    }
}
