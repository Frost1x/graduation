﻿using UnityEngine;

public class CountObj : MonoBehaviour
{
    public int objCount;
   
    void Start()
    {
        objCount = 0;
        
        if (transform.childCount == 0) {return;}

        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).childCount > 0) { objCount += transform.GetChild(i).childCount; }
            objCount++;
           
        }
      
            Debug.Log("Количество объектов и их дочерних = " + objCount);
    }
}
