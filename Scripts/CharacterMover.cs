﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMover : MonoBehaviour
{
    private InputHandler _input;
    public VectorValue startingPosition;

    [SerializeField]
    public float moveSpeed;

    [SerializeField]
    public float rotateSpeed;

    [SerializeField]
    private bool rotateTowardsMouse;

    [SerializeField]
    private Animator animator;

    private float bufferMove, bufferRotate;
    private Vector3 oldPosition;
    private Camera camera;

    private void Awake()
    {
        camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        if (!animator) { gameObject.GetComponent<Animator>(); }
        _input = GetComponent<InputHandler>();
    }

    private void Start()
    {
        bufferMove = moveSpeed;
        bufferRotate = rotateSpeed;
        transform.position = startingPosition.initialValue;
    }


    // Update is called once per frame
    void Update()
    {
        var targetVector = new Vector3(_input.InputVector.x, 0, _input.InputVector.y);

        //Find the speed
        var speed = Vector3.Distance(oldPosition, transform.position);
        oldPosition = transform.position;

        //move in the direction we are aiming
        var movementVector = MoveTowardTarget(targetVector);

        if (!rotateTowardsMouse)
            //rotate in the direction we are traveling
            RotateTowardMovementVector(movementVector);
        else
            RotateTowardMouseVector();

        animator.SetFloat("MoveSpeed", speed*15);
        animator.SetBool("Grounded", true);
    }

    private void RotateTowardMouseVector()
    {
        var ray = camera.ScreenPointToRay(_input.MousePosition);

        if(Physics.Raycast(ray, out RaycastHit hitInfo, maxDistance: 300f))
        {
            var target = hitInfo.point;
            target.y = transform.position.y;
            transform.LookAt(target);
        }
    }

    private void RotateTowardMovementVector(Vector3 movementVector)
    {
        if(movementVector.magnitude == 0) { return; }

        var rotation = Quaternion.LookRotation(movementVector);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, rotation, rotateSpeed);
    }

    private Vector3 MoveTowardTarget(Vector3 targetVector)
    {
        var speed = moveSpeed * Time.deltaTime;

        targetVector = targetVector.normalized;

        targetVector = Quaternion.Euler(0, camera.gameObject.transform.eulerAngles.y, 0) * targetVector;
        var targetPosition = transform.position + targetVector * speed;
        transform.position = targetPosition;
        return targetVector;
    }

    public void StopPlayer()
    {
        moveSpeed = 0;
        rotateSpeed = 0;
    }

    public void ResumePlayer()
    {
        moveSpeed = bufferMove;
        rotateSpeed = bufferRotate;
    }
}
