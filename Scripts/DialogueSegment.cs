﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DialogueSegment
{
    public DialogueSegment()
    {
        Answers = new List<string>();
        SegmentAfterAnswer = new List<int>();
    }

    public string DialogueText;

    public List<string> Answers;

    public List<int> SegmentAfterAnswer;
}
