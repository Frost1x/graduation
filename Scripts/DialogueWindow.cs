﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DialogueWindow : MonoBehaviour
{
    public TMP_Text dialogueText;

    private List<Button> OptionButtons;

    private Dialogue activeDialogue;

    public GameObject buttonPrefab;

    public Transform buttonParent;

    private int segmentIndex = 0;

    private GameObject player;

    private CharacterMover playerMover;

    private GameObject interactUI;

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    private void Start()
    {
        playerMover = player.GetComponent<CharacterMover>();
        var parent = transform.parent.gameObject;
        interactUI = parent.transform.GetChild(1).gameObject;

    }

    public void ExecuteDialogue(Dialogue dialogue)
    {
        this.activeDialogue = dialogue;
        UpdateDialog(activeDialogue.segments[segmentIndex]);
    }

    public void AnswerClicked(int answerIndex)
    {
        segmentIndex = activeDialogue.segments[segmentIndex].SegmentAfterAnswer[answerIndex];

        if (segmentIndex < 0)
        {
            gameObject.SetActive(false);
            segmentIndex = 0;
            playerMover.ResumePlayer();
            interactUI.SetActive(true);
        }
        else
        {
            UpdateDialog(activeDialogue.segments[segmentIndex]);
        }
    }

    private void UpdateDialog(DialogueSegment newSegment)
    {
        dialogueText.text = newSegment.DialogueText;
        int answerIndex = 0;
        foreach (Transform child in buttonParent)
        {
            Destroy(child.gameObject);
        }
        foreach (var answer in newSegment.Answers)
        {
            var btn = Instantiate(buttonPrefab, buttonParent);
            btn.GetComponentInChildren<TextMeshProUGUI>().text = answer;

            var index = answerIndex;
            btn.GetComponentInChildren<Button>().onClick.AddListener((() =>
            {
                AnswerClicked(index);
            }));

            answerIndex++;
        }
    }
}

